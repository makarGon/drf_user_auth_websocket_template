# Шаблон Django проекта с установленными celery, redis

[Назад](../../README.md)

На основе шаблона [django_linters_formatters_githooks_template](https://gitlab.com/golosovsa/django_linters_formatters_githooks_template.git)

[README.md](./donor_repository_readme/README.md) шаблона-донора

(инструкция в конце файла)

## Добавлены команды для ./manage.py

* [initserver](../../service/apps/management/management/commands/initserver.py)

## Установленные средства администрирование

* [redisboard](https://django-redisboard.readthedocs.io/en/latest/)
* [flower](https://flower.readthedocs.io/en/latest/)

## Примеры

* [кэширование кверисета (class CachedTemplateView(ListView):...)](../../service/apps/examples/views.py)
* [Асинхронные таски](../../service/apps/examples/tasks.py)
* [Асинхронные таски на стороне клиента (vanille js)](../../service/apps/examples/templates/examples/tasks.html)

## Инструкция для linux

В папке проекта
```shell
ln ./config/.env.local .env
docker compose up
```

## Инструкции для Windows

Скопировать файл `./config/.env.local` в корень проекта и переименовать в `.env`
```shell
docker compose up
```

Первый запуск занимает много времени)))

Докер контейнеры разворачиваются и настраиваются автоматически

* Создаются миграции
* Применяются миграции
* Загружаются фикстуры
* Собирается статика
* Сбрасывается axes - мало ли что, меня жутко бесит, когда dev сервер вдруг говорит что мои действия подозрительные)))
* Создается суперпользователь `dev` с паролем `DevUser1!`
* Добавляется в redisboard сервер redis

[Назад](../../README.md)
