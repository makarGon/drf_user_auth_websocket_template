# Шаблон приложения Django с установленными и настроенными линтерами, форматерами и гит хуками

[Назад](../README.md)

Почти все взято [отсюда](https://github.com/wemake-services/wemake-django-template)

По просьбам трудящихся на русском языке

(инструкция в конце файла)

## Как подготовить свое рабочее окружение

[Подготовка рабочего окружения](README/non-project-dev-tools.md)

## Инструкция по изменению dev настроек проекта

[Настройки проекта](README/template_dev_settings.md)


## Инструкция

1. Создать жесткую ссылку на файл с локальными переменными окружения
```shell
ln config/.env.local .env
```
2. Запустить в отдельном терминале
```shell
cd docker-dev
docker compose up
```
3. Запустить dev сервер
```shell
./manage.py runserver
```

## Инструкция для Windows пользователей

1. Скопировать содержимое папки `./for-windows-users/` в корень проекта
2. В отдельном терминале запустить `docker compose up`
3. В другом отдельном терминале узнать имя запущенного контейнера с джангой
```shell
docker ps
```
Выведется что-то вроде
```shell
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED         STATUS                   PORTS                    NAMES
da3030a0812e   django_linters_formatters_githooks_template-service   "python3 /home/root/…"   2 minutes ago   Up 2 minutes             0.0.0.0:8000->8000/tcp   django_linters_formatters_githooks_template-service-1
6b2df343c224   postgres:latest                                       "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes (healthy)   0.0.0.0:5432->5432/tcp   django_linters_formatters_githooks_template-db-1
```
4. Копируем имя нашего сервиса, у меня это `django_linters_formatters_githooks_template-service-1`
5. Далее запускаем терминал контейнера командой:
```shell
docker exec -it django_linters_formatters_githooks_template-service-1 bash
```
6. Переходим в папку с проектом в запущенном контейнере
```shell
cd /home/root/app/
```
7. Далее можно запускать миграции, создание суперпользователя и тп.
```shell
root@da3030a0812e:/home/root/app# ./manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, axes, contenttypes, db, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying axes.0001_initial... OK
  Applying axes.0002_auto_20151217_2044... OK
  Applying axes.0003_auto_20160322_0929... OK
  Applying axes.0004_auto_20181024_1538... OK
  Applying axes.0005_remove_accessattempt_trusted... OK
  Applying axes.0006_remove_accesslog_trusted... OK
  Applying axes.0007_alter_accessattempt_unique_together... OK
  Applying axes.0008_accessfailurelog... OK
  Applying db.0001_initial... OK
  Applying sessions.0001_initial... OK
root@da3030a0812e:/home/root/app# ./manage.py createsuperuser
Username (leave blank to use 'root'): grm
Email address: 
Password: 
Password (again): 
Superuser created successfully.
```
После применения миграций шаблон должен заработать

[Назад](../README.md)

