FROM python:3.11.3

WORKDIR /home/root/

RUN pip install --upgrade pip setuptools
RUN pip install poetry
COPY ./pyproject.toml .
RUN poetry config virtualenvs.create false && poetry install --no-root
COPY ./config/.env.local .env

