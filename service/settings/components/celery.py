from settings.components.redis import (
    REDIS_CELERY_DB,
    REDIS_HOST,
    REDIS_PORT,
)

CELERY_TIMEZONE = 'UTC'
CELERY_TASK_TRACK_STARTED = True
CELERY_TASK_TIME_LIMIT = 30 * 60

CELERY_BROKER_URL = 'redis://{host}:{port}/{db}'.format(
    host=REDIS_HOST,
    port=REDIS_PORT,
    db=REDIS_CELERY_DB,
)
CELERY_RESULT_BACKEND = 'redis://{host}:{port}/{db}'.format(
    host=REDIS_HOST,
    port=REDIS_PORT,
    db=REDIS_CELERY_DB,
)

CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
