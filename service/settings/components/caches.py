from settings.components.redis import (
    REDIS_CACHE_DB,
    REDIS_HOST,
    REDIS_PORT,
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.redis.RedisCache',
        'LOCATION': 'redis://{host}:{port}/{db}'.format(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_CACHE_DB),
    },
}

AXES_CACHE = 'default'
