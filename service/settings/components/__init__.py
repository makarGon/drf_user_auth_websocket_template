import os
from pathlib import Path

from decouple import AutoConfig

BASE_DIR = Path(__file__).resolve().parent.parent.parent
PROJECT_ROOT = BASE_DIR.parent
APPS_DIR = BASE_DIR / 'apps'
config = AutoConfig(search_path=BASE_DIR.parent)
