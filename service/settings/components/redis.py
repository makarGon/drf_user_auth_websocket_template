from settings.components import config

REDIS_HOST: str = config('DJANGO_REDIS_HOST')
REDIS_PORT: str = config('DJANGO_REDIS_PORT')
REDIS_CACHE_DB: str = config('DJANGO_REDIS_CACHE_DB')
REDIS_DB: str = config('DJANGO_REDIS_DB')
REDIS_CACHE_DB_TTL: int = int(config('DJANGO_REDIS_DB_TTL'))
REDIS_CELERY_DB: str = config('DJANGO_REDIS_CELERY_DB')
