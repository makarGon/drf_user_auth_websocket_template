from typing import Tuple

# docs https://github.com/mozilla/django-csp

CSP_SCRIPT_SRC: Tuple[str, ...] = ("'self' 'nonce-dev'",)
CSP_IMG_SRC: Tuple[str, ...] = ("'self' https://api.slingacademy.com/",)
CSP_FONT_SRC: Tuple[str, ...] = ("'self'",)
CSP_STYLE_SRC: Tuple[str, ...] = ("'self'",)
CSP_DEFAULT_SRC: Tuple[str, ...] = ("'none'",)
CSP_CONNECT_SRC: Tuple[str, ...] = ()
