import string

AUTHENTICATION_BACKENDS = (
    'axes.backends.AxesBackend',
    'django.contrib.auth.backends.ModelBackend',
)

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', 'OPTIONS': {'min_length': 8}},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# TODO: fix it
# AUTH_USER_MODEL = 'authentication.SlimUser'

RANDOM_PASSWORD_GENERATOR_CHAR_WEIGHTS = [
    {'source': string.ascii_lowercase, 'weight': 8},
    {'source': string.ascii_uppercase, 'weight': 4},
    {'source': string.digits, 'weight': 2},
    {'source': string.punctuation, 'weight': 2},
]

RANDOM_PASSWORD_GENERATOR_LENGTH = 16
RANDOM_PASSWORD_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS = 10

UNIQUE_USER_SLUG_GENERATOR_START_LENGTH = 4
UNIQUE_USER_SLUG_GENERATOR_SOURCE = string.ascii_lowercase + string.digits
UNIQUE_USER_SLUG_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS = 10

CONFIRMATION_TOKEN_TTL_SECONDS = 60 * 15
