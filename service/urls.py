from django.conf import settings
from django.contrib import admin
from django.contrib.admindocs import urls as admindocs_urls
from django.urls import include, path
from django.views.generic import RedirectView, TemplateView
from health_check import urls as health_urls

urlpatterns = [
    path('health/', include(health_urls)),

    path('admin/doc/', include(admindocs_urls)),
    path('admin/', admin.site.urls),

    path('robots.txt', TemplateView.as_view(
        template_name='txt/robots.txt',
        content_type='text/plain',
    )),
    path('humans.txt', TemplateView.as_view(
        template_name='txt/humans.txt',
        content_type='text/plain',
    )),
]

if settings.DEBUG:
    import debug_toolbar  # noqa: WPS433

urlpatterns = [
    path('__debug__/', include(debug_toolbar.urls)),
    path('', RedirectView.as_view(permanent=True, pattern_name='examples:index')),
    path('examples/', include('apps.examples.urls')),
    *urlpatterns,
]
