from typing import List, TypedDict

from django.db.models import QuerySet

from apps.examples.models import ExampleAsyncTask
from apps.examples.tasks import update_task


def get_queryset(queryset: QuerySet | None = None) -> QuerySet:
    return queryset or ExampleAsyncTask.objects.all()


def get_list_of_example_async_task_ids(
    queryset: QuerySet | None = None,
) -> List[int]:
    queryset = get_queryset(queryset)
    return [task.pk for task in queryset]


ExampleAsyncTaskType = TypedDict('ExampleAsyncTaskType', {'percentage': int, 'one_percent_delay_milliseconds': int})


def get_detailed_example_async_task(
    pk: int,
    queryset: QuerySet | None = None,
) -> ExampleAsyncTaskType | None:
    try:
        task: ExampleAsyncTask = get_queryset(queryset).get(pk=pk)
        return {'percentage': task.percentage, 'one_percent_delay_milliseconds': task.one_percent_delay_milliseconds}
    except ExampleAsyncTask.DoesNotExist:
        return None


def create_example_async_task(one_percent_delay_milliseconds: int) -> int:
    task = ExampleAsyncTask(one_percent_delay_milliseconds=one_percent_delay_milliseconds)
    task.save()
    update_task.delay(task.pk)
    return task.pk


def delete_example_async_task(pk: int):
    try:
        ExampleAsyncTask.objects.get(pk=pk).delete()
    except ExampleAsyncTask.DoesNotExist:
        pass  # noqa: WPS420
