import json
from http import HTTPStatus
from typing import Any, Dict

from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import BadRequest
from django.db.models import QuerySet
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView

from apps.examples.models import ExampleCached
from apps.examples.services import (
    create_example_async_task,
    delete_example_async_task,
    get_detailed_example_async_task,
    get_list_of_example_async_task_ids,
)


class CachedTemplateView(ListView):
    template_name = 'examples/cached.html'
    queryset: QuerySet = ExampleCached.objects.all()
    allow_empty = True

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        queryset: QuerySet = context_data['object_list']
        cache_key: str = str(queryset.query)
        cached_data = cache.get(cache_key, None)

        if cached_data:
            context_data['object_list'] = cached_data
            return context_data

        queryset_data = list(queryset)
        cache.set(
            cache_key,
            queryset_data,
            settings.REDIS_CACHE_DB_TTL,  # type: ignore
        )
        context_data['object_list'] = queryset_data

        return context_data


@require_http_methods(['GET'])
def example_tasks(request: HttpRequest) -> JsonResponse:
    data_ids = get_list_of_example_async_task_ids()
    return JsonResponse(data_ids, safe=False)


@require_http_methods(['GET'])
def example_task_detailed(request: HttpRequest, pk: int) -> JsonResponse:
    data_detailed = get_detailed_example_async_task(pk)

    if data_detailed is None:
        return JsonResponse({'error': 'Not found'})

    return JsonResponse(data_detailed)


@require_http_methods(['POST'])
def example_task_create(request: HttpRequest) -> JsonResponse:
    try:
        json_post_body: Dict[str, Any] = json.loads(request.body)
        one_percent_delay_milliseconds = int(json_post_body['one_percent_delay_milliseconds'])
    except (ValueError, KeyError, json.JSONDecodeError) as exception:
        raise BadRequest(str(exception))

    new_task_pk = create_example_async_task(one_percent_delay_milliseconds)

    return JsonResponse({'pk': new_task_pk})


@require_http_methods(['DELETE'])
def example_task_delete(request: HttpRequest, pk: int):
    delete_example_async_task(pk)
    return HttpResponse(status=HTTPStatus.OK)
