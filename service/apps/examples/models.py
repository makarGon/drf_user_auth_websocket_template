from django.db import models

MAX_TITLE_LENGTH = 256
MAX_DESCRIPTION_LENGTH = 1024
MAX_CATEGORY_LENGTH = 128


class ExampleCached(models.Model):
    title = models.CharField(max_length=MAX_TITLE_LENGTH)
    description = models.CharField(max_length=MAX_DESCRIPTION_LENGTH)
    category = models.CharField(max_length=MAX_CATEGORY_LENGTH)
    photo_url = models.URLField()
    content_text = models.TextField()
    content_html = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Data for cache'
        verbose_name_plural = 'Data for cache'

    def __str__(self):
        return 'Data for cache (user post) #{pk}'.format(pk=self.pk)


class ExampleAsyncTask(models.Model):
    percentage = models.PositiveIntegerField(default=0)
    one_percent_delay_milliseconds = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'Async task'
        verbose_name_plural = 'Async tasks'

    def __str__(self):
        return 'Async task #{pk} ({percentage}%)'.format(pk=self.pk, percentage=self.percentage)
