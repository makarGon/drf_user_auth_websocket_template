import os
from typing import List, TypedDict

from django.conf import settings
from django.core.management import BaseCommand

CREATE_SUPERUSER_SCRIPT_TEMPLATE = \
    'from django.contrib.auth.models import User\n' \
    'if not User.objects.filter(email=\'{email}\').exists():\n' \
    '    User.objects.create_superuser(\'{user}\', \'{email}\', \'{password}\')'  # noqa: N400, WPS326, Q003

ADD_REDIS_CACHE_DATABASE_SCRIPT_TEMPLATE = \
    'from redisboard.models import RedisServer; ' \
    'RedisServer.objects.get_or_create(label=\'app cache\', ' \
    'defaults=dict(url=\'redis://{host}:{port}/{db}\'))'  # noqa: N400, WPS326, Q003

ADD_REDIS_BROKER_DATABASE_SCRIPT_TEMPLATE = \
    'from redisboard.models import RedisServer; ' \
    'RedisServer.objects.get_or_create(label=\'app broker\', ' \
    'defaults=dict(url=\'redis://{host}:{port}/{db}\'))'  # noqa: N400, WPS326, Q003

_create_superuser_script = CREATE_SUPERUSER_SCRIPT_TEMPLATE.format(
    user=settings.DJANGO_SUPERUSER_USERNAME,  # type: ignore
    email=settings.DJANGO_SUPERUSER_EMAIL,  # type: ignore
    password=settings.DJANGO_SUPERUSER_PASSWORD,  # type: ignore
)

_add_redis_cache_database_script = ADD_REDIS_CACHE_DATABASE_SCRIPT_TEMPLATE.format(
    host=settings.REDIS_HOST,  # type: ignore
    port=settings.REDIS_PORT,  # type: ignore
    db=settings.REDIS_CACHE_DB,  # type: ignore
)

_add_redis_broker_database_script = ADD_REDIS_BROKER_DATABASE_SCRIPT_TEMPLATE.format(
    host=settings.REDIS_HOST,  # type: ignore
    port=settings.REDIS_PORT,  # type: ignore
    db=settings.REDIS_CELERY_DB,  # type: ignore
)


CommandInfo = TypedDict('CommandInfo', {'action': str, 'code': int})


class Command(BaseCommand):
    help = 'Initialize dev server'
    output_line_length = 60

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.executed_commands: List[CommandInfo] = []

    def handle(self, *args, **kwargs) -> None:  # noqa: WPS110
        self.clear_executed_commands()

        self.handle_management_commands()
        self.handle_python_scripts()

        self.print_executed_commands_info()

    def handle_management_commands(self):
        self.execute_command('Make migrations', 'python3 manage.py makemigrations')
        self.execute_command('Migrate', 'python3 manage.py migrate')
        self.execute_command('Load data', 'python3 manage.py loaddata ExampleCached.json')
        self.execute_command('Collect static', 'python3 manage.py collectstatic --no-input')
        self.execute_command('Reset axes', 'python3 manage.py axes_reset')

    def handle_python_scripts(self):
        self.execute_command(
            'Create superuser',
            'python3 manage.py shell -c \"{script}\"'.format(script=_create_superuser_script),
        )
        self.execute_command(
            'Add to redisboard cache server',
            'python3 manage.py shell -c \"{script}\"'.format(script=_add_redis_cache_database_script),
        )
        self.execute_command(
            'Add to redisboard broker server',
            'python3 manage.py shell -c \"{script}\"'.format(script=_add_redis_broker_database_script),
        )

    def execute_command(self, action: str, command: str):
        code = os.system(command)  # noqa: S605
        self.executed_commands.append({'action': action, 'code': code})

    def clear_executed_commands(self) -> None:
        self.executed_commands.clear()

    def print_executed_commands_info(self) -> None:
        print('\n\n')  # noqa: WPS421
        for command_info in self.executed_commands:
            action = command_info['action']
            status = '\033[92mOK\033[0m' if command_info['code'] == 0 else '\033[31mFailed\033[0m'
            dots_count = (self.output_line_length - len(action) - len(status) - 2)
            print(  # noqa: WPS421
                '{action} {dots} {status}'.format(action=action, dots='.' * dots_count, status=status),
            )
        print('\n')  # noqa: WPS421
