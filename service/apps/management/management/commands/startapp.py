from pathlib import Path
from typing import Any

from django.conf import settings
from django.core.management import CommandError
from django.core.management.templates import TemplateCommand


class Command(TemplateCommand):

    def handle(  # noqa: WPS110
        self,
        app_or_project: str | None = None,
        name: str | None = None,
        target: str | None = None,
        **options: Any,
    ) -> None:

        directory = options.pop('directory')
        app_name: str = name if name else options.pop('name')
        app_directory: Path = settings.APPS_DIR  # type: ignore

        if directory:
            app_directory /= directory
        if app_name:
            app_directory /= app_name

        if app_directory.exists():
            raise CommandError("'{0}' already exists".format(app_directory))

        app_directory.mkdir(parents=True, exist_ok=False)

        super().handle('app', app_name, target=str(app_directory), **options)
