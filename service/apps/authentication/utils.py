import random
import string
from operator import itemgetter
from typing import TypedDict, Callable

from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import IntegrityError

DISPLAYABLE_CHARS = string.ascii_letters + string.digits + string.punctuation


def generate_random_string(length: int, source=DISPLAYABLE_CHARS) -> str:
    return random.sample(source, length)


CharWeight = TypedDict('CharWeight', {'source': str, 'weight': float})


def generate_random_string_from_char_weights(length: int, char_weights: list[CharWeight]) -> str:
    total_weight: float = sum(map(itemgetter('weight'), char_weights))
    random_samples = [
        generate_random_string(
            length=int(length * char_weight['weight'] / total_weight),
            source=char_weight['source'],
        ) for char_weight in char_weights
    ]
    return random.sample(''.join(random_samples), length)


RANDOM_PASSWORD_GENERATOR_CHAR_WEIGHTS: list[CharWeight] = settings.RANDOM_PASSWORD_GENERATOR_CHAR_WEIGHTS
RANDOM_PASSWORD_GENERATOR_LENGTH: int = settings.RANDOM_PASSWORD_GENERATOR_LENGTH
RANDOM_PASSWORD_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS: int = \
    settings.RANDOM_PASSWORD_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS


def generate_random_password(length=RANDOM_PASSWORD_GENERATOR_LENGTH) -> str:
    return generate_random_string_from_char_weights(
        length,
        RANDOM_PASSWORD_GENERATOR_CHAR_WEIGHTS
    )


def generate_valid_password() -> str:
    for _ in range(RANDOM_PASSWORD_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS):
        try:
            password = generate_random_password()
            validate_password(password)
        except ValidationError:
            pass
        else:
            return password
    raise IntegrityError('Exceeded the maximum number of attempts to generate a valid password')


UNIQUE_USER_SLUG_GENERATOR_START_LENGTH = settings.UNIQUE_USER_SLUG_GENERATOR_START_LENGTH
UNIQUE_USER_SLUG_GENERATOR_SOURCE = settings.UNIQUE_USER_SLUG_GENERATOR_SOURCE
UNIQUE_USER_SLUG_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS = \
    settings.UNIQUE_USER_SLUG_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS


def generate_unique_slug(
    uniqueness_validation_function: Callable[[str], bool],
    start_length: int = UNIQUE_USER_SLUG_GENERATOR_START_LENGTH,
) -> str:
    for iteration in range(UNIQUE_USER_SLUG_GENERATOR_MAX_NUMBER_OF_VALIDATION_ATTEMPTS):
        slug = generate_random_string(
            start_length + iteration,
            source=UNIQUE_USER_SLUG_GENERATOR_SOURCE
        )
        if uniqueness_validation_function(slug):
            return slug
    raise IntegrityError('Exceeded the maximum number of attempts to generate a unique slug')
