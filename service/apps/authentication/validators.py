from typing import Any

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import phonenumbers


class MobilePhoneValidator:
    message = _('Ensure this value %(value) is correct mobile phone number')
    code = 'invalid'

    def __init__(
        self,
        code: str | None = None,
        message: str | None = None,
    ):
        if code:
            self.code = code
        if message:
            self.message = message

    def __call__(self, value: str):
        try:
            phonenumbers.parse(value)
        except phonenumbers.NumberParseException:
            raise ValidationError(self.message, code=self.code, params={'value': value})

    def __eq__(self, other: Any):
        return (
            isinstance(other, MobilePhoneValidator)
            and (self.code == other.code)
            and (self.message == other.message)
        )
