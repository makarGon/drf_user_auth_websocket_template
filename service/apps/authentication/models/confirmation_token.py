import uuid

from django.db import models
from slim_user import SlimUser

from django.utils.translation import gettext_lazy as _


class ConfirmationToken(models.Model):
    uuid = models.UUIDField(
        verbose_name=_('UUID uuid'),
        default=None,
    )
    expired = models.DateTimeField(
        verbose_name=_('Expired to'),
        default=None,
    )
    user = models.OneToOneField(
        to=SlimUser,
        related_name='token',
        on_delete=models.SET_NULL,
        primary_key=True,
    )

    class Meta:
        verbose_name = _('Confirmation uuid')
        verbose_name_plural = _('Confirmation tokens')
        indexes = [
            models.Index(fields=['user']),
            models.Index(fields=['user', 'uuid'])
        ]
        unique_together = [
            ['user', 'uuid'],
        ]
