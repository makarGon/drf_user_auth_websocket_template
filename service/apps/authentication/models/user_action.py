from datetime import datetime, timedelta
from django.utils.translation import gettext_lazy as _

from django.db import models

from .slim_user import SlimUser

from django.contrib.contenttypes.fields import GenericForeignKey


class UserAction(models.Model):
    timestamp = models.DateTimeField(
        verbose_name=_('Timestamp'),
        auto_now_add=True,
    )
    user = models.ForeignKey(
        to=SlimUser,
        on_delete=models.SET_NULL,
        related_name='actions'
    )
    description = models.CharField(
        max_length=255,
        default=_('{user} user takes action to {target}'),
        null=False,
        blank=False
    )
    target_id = models.PositiveIntegerField()
    target = GenericForeignKey(
        'content_type',
        'target_id',
        for_concrete_model=False
    )

    def __str__(self):
        return self.description.format(user=self.user.email, target=str(self.target))

    class Meta:
        verbose_name = 'User action'
        verbose_name_plural = 'User actions'
        indexes = [
            models.Index(fields=['user', 'timestamp']),
            models.Index(fields=['timestamp']),
        ]
