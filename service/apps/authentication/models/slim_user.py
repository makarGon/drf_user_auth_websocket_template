from typing import Iterable

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

from .slim_user_manager import SlimUserManager


class SlimUser(AbstractBaseUser, PermissionsMixin):
    slug = models.SlugField(unique=True, blank=False, null=False)

    email = models.EmailField(_("email address"), unique=True, blank=False, null=False)
    mobile_phone_number = models.CharField(_('mobile phone number'), max_length=30, default=None)
    is_staff = models.BooleanField(_('Is staff'), default=False)
    is_active = models.BooleanField(_('Is active'), default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = SlimUserManager()

    class Meta:
        verbose_name = _('Slim user')
        verbose_name_plural = _('Slim users')
        indexes = [
            models.Index(fields=['slug']),
            models.Index(fields=['email']),
        ]

    def __str__(self):
        return '{username} ({status})'.format(
            username=self.email,
            status=_('active') if self.is_active else _('banned')
        )

    @property
    def is_admin(self):
        return self.is_staff and self.is_active
