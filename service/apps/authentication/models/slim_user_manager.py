from django.contrib.auth.base_user import BaseUserManager

from ..utils import generate_valid_password, generate_unique_slug


class SlimUserManager(BaseUserManager):
    def create_user(self, email: str, password: str | None = None):
        extra_fields: dict[str, str | bool | None] = dict(
            mobile_phone_number=None,
            is_staff=False,
            is_active=True,
        )

        if password is None or password == '':
            password = generate_valid_password()

        self._create_user(email, password, **extra_fields)

    def create_superuser(self, email: str, password: str):
        extra_fields: dict[str, str | bool | None] = dict(
            mobile_phone_number=None,
            is_staff=True,
            is_active=True,
        )
        self._create_user(email, password, **extra_fields)

    def _create_user(self, email: str, password: str, **extra_fields):
        slug = self.generate_slug()
        user = self.model(email=email, slug=slug, **extra_fields)
        user.set_password(password)
        user.save()

    def generate_slug(self) -> str:
        return generate_unique_slug(
            lambda slug: not self.filter(slug=slug).exist(),
        )
