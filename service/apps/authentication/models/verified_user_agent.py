from datetime import timedelta, datetime

from django.db import models
from user_agents import parse
from .slim_user import SlimUser


class VerifiedUserAgent(models.Model):
    user_agent = models.CharField(max_length=255)
    user = models.ForeignKey(to=SlimUser, on_delete=models.SET_NULL, related_name='user_agents')
    expired = models.DateTimeField()

    class Meta:
        verbose_name = 'Verified user agent'
        verbose_name_plural = 'Verified user agents'
        indexes = [
            models.Index(fields=['user', 'user_agent']),
        ]
        unique_together = [
            ['user', 'user_agent'],
        ]

    def __str__(self):
        parsed_user_agent = parse(self.user_agent)
        return '{user} - [{device}, {os}]{expired}'.format(
            user=self.user.email,
            device=parsed_user_agent.device,
            os=parsed_user_agent.os,
            expired='' if datetime.now() < self.expired else ' (expired)',
        )
